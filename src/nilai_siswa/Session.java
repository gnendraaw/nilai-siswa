/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package nilai_siswa;

/**
 *
 * @author ASUS
 */
public class Session {    
    private static String idLogin = "";
    private static String level = "";
    
    public Session() 
    {
        idLogin = "";
        level = "";
    }
    
    public static void setIdLogin(String id)
    {
        Session.idLogin = id;
    }
    
    public static void setLevel(String level)
    {
        Session.level = level;
    }
    
    public static String getIdLogin()
    {
        return Session.idLogin;
    }
    
    public static String getLevel()
    {
        return Session.level;
    }
}
