/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JInternalFrame.java to edit this template
 */
package nilai_siswa;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ASUS
 */
public class FormJurusan extends javax.swing.JInternalFrame {
    String textKode, textNama, buffNama;
    KoneksiDB db = new KoneksiDB();
    Connection con;
    DefaultTableModel tm;

    /**
     * Creates new form FormJurusan
     */
    public FormJurusan() {
        initComponents();
        connect();
        refreshTable();
    }
    
    private void connect()
    {
        con = db.connect();
    }
    
    private void refreshTable()
    {
        tm = new DefaultTableModel(
            null,
                new Object[] {"Kode Jurusan", "Nama Jurusan" }
        );
        tbMapel.setModel(tm);
        tm.getDataVector().removeAllElements();
        
        try {
            PreparedStatement s = con.prepareStatement("SELECT * FROM jurusan");
            ResultSet r = s.executeQuery();
            while(r.next()) {
                Object[] data = {
                    r.getString(1),
                    r.getString(2),
                };
                tm.addRow(data);
            }
        } catch (Exception e) {
            System.out.println("ERROR QUERY KE DATABASE:\n" + e + "\n\n");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        tfKodeJurusan = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        tfNamaJurusan = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbMapel = new javax.swing.JTable();
        tambahBtn = new javax.swing.JButton();
        hapusBtn = new javax.swing.JButton();
        ubahBtn = new javax.swing.JButton();

        setClosable(true);
        setMaximizable(true);
        setTitle("FORM JURUSAN");

        jLabel1.setFont(new java.awt.Font("Open Sans", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("DATA JURUSAN");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel2.setText("Kode Jurusan");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel3.setText("Nama Jurusan");

        tbMapel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tbMapel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbMapel.setShowGrid(true);
        tbMapel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbMapelMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbMapel);

        tambahBtn.setText("TAMBAH");
        tambahBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tambahBtnActionPerformed(evt);
            }
        });

        hapusBtn.setText("HAPUS");
        hapusBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusBtnActionPerformed(evt);
            }
        });

        ubahBtn.setText("UBAH");
        ubahBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ubahBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(45, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(35, 35, 35)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfNamaJurusan, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfKodeJurusan, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(tambahBtn)
                        .addGap(18, 18, 18)
                        .addComponent(ubahBtn)
                        .addGap(18, 18, 18)
                        .addComponent(hapusBtn))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 381, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 45, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(22, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfKodeJurusan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfNamaJurusan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tambahBtn)
                    .addComponent(hapusBtn)
                    .addComponent(ubahBtn))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tbMapelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbMapelMouseClicked
        // TODO add your handling code here:
        tfKodeJurusan.setText(tm.getValueAt(tbMapel.getSelectedRow(), 0).toString());
        tfNamaJurusan.setText(tm.getValueAt(tbMapel.getSelectedRow(), 1).toString());
    }//GEN-LAST:event_tbMapelMouseClicked

    private void tambahBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tambahBtnActionPerformed
        // TODO add your handling code here:
        try {
            PreparedStatement ps = con.prepareStatement("INSERT INTO jurusan VALUES (?, ?)");
            ps.setString(1, tfKodeJurusan.getText());
            ps.setString(2, tfNamaJurusan.getText());
            ps.executeUpdate();
            
            refreshTable();
            tfKodeJurusan.setText("");
            tfNamaJurusan.setText("");
        } catch(Exception e) {
            System.out.print("ERROR QUERY KE DATABASE:\n" + e + "\n\n");
        }
    }//GEN-LAST:event_tambahBtnActionPerformed

    private void ubahBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ubahBtnActionPerformed
        // TODO add your handling code here:
        try {
            PreparedStatement ps = con.prepareStatement("UPDATE jurusan SET namajurusan=? WHERE kodejurusan=?");
            ps.setString(1, tfNamaJurusan.getText());
            ps.setString(2, tfKodeJurusan.getText());
            ps.executeUpdate();
            
            refreshTable();
            tfKodeJurusan.setText("");
            tfNamaJurusan.setText("");
        } catch (Exception e) {
            System.out.print("ERROR QUERY KE DATABASE:\n" + e + "\n\n");
        }
    }//GEN-LAST:event_ubahBtnActionPerformed

    private void hapusBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusBtnActionPerformed
        // TODO add your handling code here:
        try {
            PreparedStatement ps = con.prepareStatement("DELETE FROM jurusan WHERE kodejurusan=?");
            ps.setString(1, tfKodeJurusan.getText());
            ps.executeUpdate();
            
            refreshTable();
            tfKodeJurusan.setText("");
            tfNamaJurusan.setText("");
        } catch (Exception e) {
            System.out.print("ERROR QUERY KE DATABASE:\n" + e + "\n\n");
        }
    }//GEN-LAST:event_hapusBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton hapusBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton tambahBtn;
    private javax.swing.JTable tbMapel;
    private javax.swing.JTextField tfKodeJurusan;
    private javax.swing.JTextField tfNamaJurusan;
    private javax.swing.JButton ubahBtn;
    // End of variables declaration//GEN-END:variables
}
